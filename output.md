Роботехнический конструктор для соревнований.

Сервер на базе Raspbian.

Подключается разнообразная периферия по сетям I2C, CAN, RS-485

Доступ:

для разработчиков - SSH, Web-интерфейс

для жюри - Web-интерфейс с ограниченными правами

Для этого выполним следующие шаги

**Установка Raspbian**

У меня нету в наличии Raspberry Pi, поэтому было принято решение
сэмулировать Raspbian через QEMU

Потребовался сам QEMU, файл образа Raspbian и ядро

![](./myMediaFolder/media/image1.png){width="6.38461832895888in"
height="1.1040288713910762in"}

Конфиг для QEMU:

![](./myMediaFolder/media/image2.png){width="6.5in" height="3.4in"}

Чтобы установить apache пришлось увеличить место для этого
воспользовался этими инструкциями

Увеличение места для QEMU

<https://raspberrypi.stackexchange.com/questions/92251/sda2-is-not-on-sd-card-dont-know-how-to-expand/102097#102097>

и

<https://github.com/nachoparker/qemu-raspbian-network/issues/12>

Место увеличено:

![](./myMediaFolder/media/image3.png){width="6.5in"
height="5.322222222222222in"}

Веб сервер запущен

![](./myMediaFolder/media/image4.png){width="6.5in"
height="5.322222222222222in"}

Включил I2C

![](./myMediaFolder/media/image5.png){width="6.5in"
height="3.907638888888889in"}

Включил SSH:

![](./myMediaFolder/media/image6.png){width="6.499211504811899in"
height="4.956521216097988in"}
